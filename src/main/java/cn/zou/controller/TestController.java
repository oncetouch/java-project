package cn.zou.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author artmaster
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/info")
    public String getInfo(){
        return "Hello world！";
    }

}
