package cn.zou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author artmaster
 */
@SpringBootApplication
public class TestDemoApplication {

    public static void main(String[] args) {
        new SpringApplication(TestDemoApplication.class).run(args);
    }

}
